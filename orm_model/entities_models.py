"""Classes for entities of project."""

from pynamodb.attributes import UnicodeAttribute
from pynamodb.models import Model

from constants import AWS_DYNAMODB_TABLE, AWS_REGION


class Clients(Model):
    """Class for clients."""

    class Meta:
        """Class Meta."""

        table_name = AWS_DYNAMODB_TABLE
        region = AWS_REGION

    PK = UnicodeAttribute(hash_key=True)
    SK = UnicodeAttribute(range_key=True)
    name = UnicodeAttribute(default='')
    email = UnicodeAttribute(default='')
    status = UnicodeAttribute(default='')


class Providers(Model):
    """Class for providers."""

    class Meta:
        """Class Meta."""

        table_name = AWS_DYNAMODB_TABLE
        region = AWS_REGION

    PK = UnicodeAttribute(hash_key=True)
    SK = UnicodeAttribute(range_key=True)
    name = UnicodeAttribute(default='')
    email = UnicodeAttribute(default='')
    status = UnicodeAttribute(default='')


class Categories(Model):
    """Class for categories."""

    class Meta:
        """Class Meta."""

        table_name = AWS_DYNAMODB_TABLE
        region = AWS_REGION

    PK = UnicodeAttribute(hash_key=True)
    SK = UnicodeAttribute(range_key=True)
    name = UnicodeAttribute(default='')


class Services(Model):
    """Class for services."""

    class Meta:
        """Class Meta."""

        table_name = AWS_DYNAMODB_TABLE
        region = AWS_REGION

    PK = UnicodeAttribute(hash_key=True)
    SK = UnicodeAttribute(range_key=True)
    name = UnicodeAttribute(default='')
    price = UnicodeAttribute(default='')
    categorie_id = UnicodeAttribute(default='')


class PurchasesClients(Model):
    """Class for purchases of clients."""

    class Meta:
        """Class Meta."""

        table_name = AWS_DYNAMODB_TABLE
        region = AWS_REGION

    PK = UnicodeAttribute(hash_key=True)
    SK = UnicodeAttribute(range_key=True)
    provider_id = UnicodeAttribute(default='')
    price_net = UnicodeAttribute(default='')


class PurchesesProviders(Model):
    """Class for purchases of providers."""

    class Meta:
        """Class Meta."""

        table_name = AWS_DYNAMODB_TABLE
        region = AWS_REGION

    PK = UnicodeAttribute(hash_key=True)
    SK = UnicodeAttribute(range_key=True)
    price_net = UnicodeAttribute(default='')
