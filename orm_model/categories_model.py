"""Model for categories."""

from orm_model import entities_models

Categories = entities_models.Categories


def create_categorie(identifier, name):
    """Function for create categories

    Args:
        identifier: string
        name: string

    Returns:
        Categorie: A dictionary if the categorie is created
        None: If the categorie is not created
    """
    sk = 'categories-{0}'.format(identifier)
    categorie = Categories('categories', sk, name=name)
    try:
        categorie.save()
    except Exception:
        return None
    return {
        'categorie:_pk': identifier,
        'client_sk': sk,
        'name': name,
    }


def get_categorie(categorie_id):
    """Function for create categories

    Args:
        identifier: string
        name: string

    Returns:
        Categorie: A dictionary if the categorie is created
    """
    sk = 'categories-{0}'.format(categorie_id)
    try:
        categorie = Categories.get('categories', sk)
    except Exception:
        return None
    return categorie


def get_categories():
    """Functioón for get all clients."""
    categories = []
    try:
        for categorie in Categories.query('categories', Categories.sk.startswith('categories-')):
            categories.append(
                {
                    'categorie_id': categorie.sk.split('-')[1],
                    'name': categorie.name,
                },
            )
    except Exception:
        return None
    return categories
