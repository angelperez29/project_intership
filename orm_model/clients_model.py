"""Model for administration clients."""

import logging

from orm_model import entities_models

Clients = entities_models.Clients

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def create(identifier, name, email):
    """Creation of a new client.

    Args:
        identifier: string
        name: string
        email: string

    Returns:
        Dict with information of client
        Tuple with a none and exception as a str in case a error
    """
    sk = 'clients-{0}'.format(identifier)
    response = Clients('clients', sk, name=name, email=email, status='active')
    try:
        response.save()
    except Exception as client_exception:
        logger.exception(client_exception)
        return str(client_exception)
    return {
        'client_pk': 'clients',
        'client_sk': sk,
        'name': name,
        'email': email,
        'status': 'active',
    }


def get(client_id):
    """Get a single client.

    Args:
        client_id:  string

    Returns:
        - Dict with information of client
        - Tuple with a none and exception as a str in case a error
    """
    sk = 'clients-{0}'.format(client_id)
    try:
        client_found = Clients.get('clients', sk)
    except Exception as client_exception:
        logger.exception(client_exception)
        return (None, client_exception)
    return {
        'client_id': client_id,
        'name': client_found.name,
        'email': client_found.email,
        'status': client_found.status,
    }


def get_all():
    """Get all clients.

    Returns:
        - Tuple with None and Exception as string in case of error
        - clients_db a list of clients
    """
    clients_db = []
    try:
        for client in Clients.query('clients', Clients.SK.startswith('clients-')):
            clients_db.append(
                {
                    'client_id': client.SK.split('-')[1],
                    'name': client.name,
                    'email': client.email,
                },
            )
    except Exception as client_exception:
        logger.exception(client_exception)
        return (None, client_exception)
    return clients_db


def update(client_data, is_active):
    """Update a single client.

    Args:
        client_data: A dic with all information of client
        is_active: A boolean for know if client is active or not

    Returns:
        - None in case a Exception
        - client a list of clients
    """
    client_id = client_data.get('id_client')
    sk = 'clients-{0}'.format(client_id)
    try:
        client = Clients.get('clients', sk)
    except Exception as get_exception:
        logger.exception(get_exception)
        return (None, str(get_exception))
    if is_active:
        try:
            client.update(
                actions=[
                    Clients.name.set(client_data.get('name')),
                    Clients.email.set(client_data.get('email')),
                ],
            )
        except Exception as update_exception:
            logger.exception(update_exception)
            return (None, str(update_exception))
        return {
            '_id': client_id,
            'name': client.name,
            'email': client.email,
            'status': client.status,
        }

    try:
        client.update(
            actions=[
                Clients.status.set('inactive'),
            ],
        )
    except Exception as delete_exception:
        logger.exception(delete_exception)
        return (None, str(delete_exception))
    return {
        '_id': client_id,
        'name': client.name,
        'email': client.email,
        'status': client.status,
    }
