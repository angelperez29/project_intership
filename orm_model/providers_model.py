"""Model for Providers."""

from orm_model import entities_models

Providers = entities_models.Providers


def create_proveidentifierers(identifier, name, email, passwd):
    """Function for create clients

    Args:
        identifier: string
        name: string
        email: string
        passwd: string

    Returns:
        Provider: A dictionary if the Provider is created
        None: If the Provider is not created
    """
    sk = 'Providers-{0}'.format(identifier)
    provider = Providers('Providers', sk, name=name, email=email, passwd=passwd)
    try:
        provider.save()
    except Exception:
        return None
    return {
        'provider_pk': identifier,
        'provider_sk': sk,
        'name': name,
        'email': email,
        'passwd': passwd,
        'status': 'active',
    }


def get_provider(provider_id):
    """Gets and returns a provider from DynamoDB making use of the Clients Object with the necessary configuration for the connection.

    This function obtains from the database the requested customer given an identifier:

    Parameters
    ----------
    providers_id -- string

    Returns:
    ----------
    provider -- A dictionary if the client is created
    None -- If the client is not created
    """
    sk = 'providers-{0}'.format(provider_id)
    try:
        provider = Providers.get('providers', sk)
    except Exception:
        return None
    return {
        'provider_id': provider_id,
        'name': provider.name,
        'email': provider.email,
        'passwd': provider.passwd,
        'status': provider.status,
    }


def get_all_provders():
    """Functioón for get all clients."""
    providers = []
    try:
        for provider in Providers.query('provides', Providers.sk.startswith('providers-')):
            providers.append(
                {
                    'provider_id': provider.sk.split('-')[1],
                    'name': provider.name,
                    'email': provider.email,
                },
            )
    except Exception:
        return None
    return providers
