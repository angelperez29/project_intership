"""Model for services."""

from orm_model import entities_models

Services = entities_models.Services


def create_service(identifier, name, price, categorie_id):
    """Function for create clients.

    parameters:
        identifier: string
        name: string
        email: string
        categorie_id: string

    Returns:
        Service: A dictionary if the client is created
        None: If the client is not created
    """
    sk = 'services-{0}'.format(identifier)
    service = Services('services', sk, name, price, categorie_id)
    try:
        service.save()
    except Exception:
        return None
    return {
        'service_pk': 'services',
        'service_sk': sk,
        'name': name,
        'price': price,
        'categorie_id': categorie_id,
    }


def create_service_provider(provider_id, identifier, name, price, categorie_id):
    """Function for create clients.

    parameters:
        identifier: string
        name: string
        email: string
        categorie_id: string

    Returns:
        Client: A dictionary if the client is created
        None: If the client is not created
    """
    sk = 'service-{0}'.format(identifier)
    service = Services(provider_id, sk, name, price, categorie_id)
    try:
        service.save()
    except Exception:
        return None
    return {
        'service_pk': provider_id,
        'service_sk': identifier,
        'name': name,
        'price': price,
        'categorie_id': categorie_id,
    }


def get_services():
    """Funtion for get all service from DynamoDB."""
    try:
        services = [
            {
                'service_id': service.sk.split('-')[1],
                'name': service.name,
                'categorie_id': service.categorie_id,
            }
            for service in Services.query('services', Services.sk.startswith('services-'))
        ]
    except Exception:
        return None
    return services


def get_services_provider(provider_id):
    """Function to obtain all services given by a provider."""
    try:
        services = [
            {
                'provider_id': service.sk.split('-')[1],
                'name': service.name,
                'categorie_id': service.categorie_id,
            }
            for service in Services.query(provider_id, Services.sk.startswith('services-'))
        ]
    except Exception:
        return None
    return services
