"""Module to get all providers."""

from orm_model import providers_model
from responses_status.helper import response_helper

from project.constants import FAILED, OK


def lambda_handler(events, context):
    """Sample pure Lambda function.

    Parameters
    ----------
    event: dict, required
        API Gateway Lambda Proxy Input Format

        Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format

    context: object, required
        Lambda Context runtime methods and attributes

        Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns
    API Gateway Lambda Proxy Output Format: dict

        Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
    """
    provider = providers_model.get_all_provders()
    if provider:
        return response_helper(OK, {'items': provider})
    return response_helper(FAILED, {'message': 'Provider not found'})
