"""Module for create providers."""

import json

from orm_model import providers_model
from responses_status.helper import response_helper
from ulid import ULID


def lambda_handler(events, context):
    """Sample pure Lambda function

    Parameters
    ----------
    event: dict, required
        API Gateway Lambda Proxy Input Format

        Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format

    context: object, required
        Lambda Context runtime methods and attributes

        Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns
    API Gateway Lambda Proxy Output Format: dict

        Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
    """
    body = json.loads(events.get('body'))

    if body.get('name') and body.get('email') and body.get('passwd'):
        name = body.get('name')
        email = body.get('email')
        passwd = body.get('passwd')
        provider = providers_model.create_proveiders(str(ULID()), name, email, passwd)
        if provider:
            status_code = 200
            return response_helper(status_code, {'message': 'Provider created', 'item': provider})
    status_code = 400
    return response_helper(status_code, {'message': 'Incorrect data body'})
