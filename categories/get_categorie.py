"""Module for get a single categorie."""

from orm_model.categories_model import get_categorie
from responses_status.helper import response_helper

from constants import OK, FAILED


def lambda_handler(events, context):
    """Sample pure Lambda function.

    Parameters
    ----------
    event: dict, required
        API Gateway Lambda Proxy Input Format

        Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format

    context: object, required
        Lambda Context runtime methods and attributes

        Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns
    API Gateway Lambda Proxy Output Format: dict

        Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
    """
    try:
        categorie_id = events.get('pathParameters').get('id_categorie')
    except Exception:
        return response_helper(FAILED, {'message': 'No parameters'})
    categire = get_categorie(categorie_id)
    if categire:
        return response_helper(OK, {'item': categire})
    return response_helper(FAILED, {'message': 'categorie not found'})
