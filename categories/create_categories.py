"""Module for create categories."""

import json

from orm_model import categories_model
from responses_status.helper import response_helper
from ulid import ULID
from project.constants import OK, FAILED


def lambda_handler(events, context):
    """Sample pure Lambda function

    Parameters
    ----------
    event: dict, required
        API Gateway Lambda Proxy Input Format

        Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format

    context: object, required
        Lambda Context runtime methods and attributes

        Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns
    API Gateway Lambda Proxy Output Format: dict

        Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
    """
    body = json.loads(events.get('body'))

    if body.get('name'):
        name = body.get('name')
        categorie = categories_model.create_categorie(str(ULID()), name)
        if categorie:
            return response_helper(
                OK,
                {
                    'message': 'Categorie created',
                    'item': categorie,
                },
            )
    return response_helper(
        FAILED,
        {
            'message': 'Error when creating the client',
        },
    )
