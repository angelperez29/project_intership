"""Module for update a single client."""
import json
import logging

import constants
from orm_model import clients_model
from responses_status.helper import make_response

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def lambda_handler(events, context):
    """Lambda executes the controller method. If the controller exists or returns a response, it becomes available to handle another event.

    Args:
        events: dict, required
            Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format
        context: bject, required
            Lambda Context runtime methods and attributes
            Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns:
        dict: API Gateway Lambda Proxy Output Format
        doc https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
    """
    id_request = context.aws_request_id
    body = json.loads(events.get('body'))
    if body.get('name') and body.get('email') and body.get('passwd'):
        idclient = events.get('pathParameters').get('idclient')
        response = clients_model.update(
            client_data={
                'id_client': idclient,
                'name': body.get('name'),
                'email': body.get('email'),
            },
            is_active=True,
        )
        if response is not tuple:
            return make_response(
                status_code=constants.OK,
                id_request=id_request,
                message='The client has been updated',
                item=response,
            )
        return make_response(
            status_code=constants.FAILED,
            id_request=id_request,
            message='The client has not been updated',
            error=response[1],
        )
    return make_response(
        status_code=constants.FAILED,
        id_request=id_request,
        message='Incorrect body data',
        error='Missing data',
    )
