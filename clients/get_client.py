"""Module for get a single clients."""
import logging

import constants
from orm_model import clients_model
from responses_status.helper import make_response

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def lambda_handler(events, context):
    """Lambda executes the controller method. If the controller exists or returns a response, it becomes available to handle another event.

    Args:
        events: dict, required
            Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format
        context: bject, required
            Lambda Context runtime methods and attributes
            Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns:
        dict: API Gateway Lambda Proxy Output Format
        doc https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
    """
    try:
        client_id = events.get('pathParameters').get('idclient')
    except Exception as parameter_exception:
        logger.exception(parameter_exception)
        return make_response(
            status_code=constants.FAILED,
            id_request=context.aws_request_id,
            message='You must specify an id as parameter',
            error=str(parameter_exception),
        )

    client = clients_model.get(client_id)
    if client is not tuple:
        return make_response(
            status_code=constants.OK,
            id_request=context.aws_request_id,
            message='Client successfully found',
            item=client,
        )
    return make_response(
        status_code=constants.FAILED,
        id_request=context.aws_request_id,
        message='Client not found with id:{0}'.format(client_id),
        error='Error {0}'.format(client[1]),
    )
