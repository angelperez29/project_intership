"""Module for sing up clients on Cognito."""

import logging

import boto3

from constants import COGNITO_USER_POOL_CLIENT

client = boto3.client('cognito-idp')
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def sign_up(email, passwd):
    """User registration in Cognito.

    Args:
        email: string
        passwd: string

    Returns:
        Dict with the confirmation on registration
    """
    try:
        response = client.sign_up(
            ClientId=COGNITO_USER_POOL_CLIENT,
            Username=email,
            Password=passwd,
            UserAttributes=[
                {
                    'Name': 'email',
                    'Value': email,
                },
            ],
        )
    except Exception as sign_up_exception:
        logger.exception(sign_up_exception)
        return str(sign_up_exception)

    return dict(response)
